import {inject} from 'aurelia-framework';
import TweetService from '../../services/tweet-service';

@inject(TweetService)
export class SendTweet {

  tweetText = '';

  constructor(ts) {
    this.tweetService = ts;
  }

  sendTweet() {
    this.tweetService.addTweet(new Date(), this.tweetText);
    console.log('New tweet: ' + this.tweetText);
    this.tweetText = '';
  }
}
