# MyTweet #

MyTweet is a basic twitter-like built in aurelia (js) android application whereby a user can signup, login, send tweets, and view tweets on a timeline.

## Accessing app ##

With aurelia installed, navigate to the folder of the app via the terminal, and run the command 'au run --watch'. The app may then be navigated through in a browser locally, at localhost:9000

Once running, access to the app is via a username and password, which can be gained by registering/ signing up a user, or using one of the pre-existing users. 

Details of sample pre-existing users:

* username : homer@simpson.com
* password : secret

## Branches/ Structure ##

Repo follows a gitflow-like branch & merging structure, with the following branch types:

* features : ft1_xxx, ft2_xxx, etc
* dev : finished feature branches are merged back to dev
* master : finished development on dev is merged back to master

All work is done on individual feature branches, with finished features merged back to dev, and release candidates from dev merged back to master when finished.

## Story Tags ##

Development of the app has been via the below functionality stories:

* st_00 : Adds aurelia baseline code
* st_01 : Adds tweet timeline & send tweet func
* st_02 : Adds login/ signup functionality
* rc_01 : 1st aurelia implementation of MyTweet